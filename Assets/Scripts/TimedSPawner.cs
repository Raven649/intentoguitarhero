﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedSPawner : MonoBehaviour
{
    public GameObject noteSpawn;
    public bool stopSpawning;
    public int firstCheck;
    public float randomSpawnTime;
    public float spawnDelay;
    void Update()
    {
        randomSpawnTime =0f;
        spawnDelay = Random.Range(0.5f, 15f);
        InvokeRepeating("SpawnNote", spawnDelay, randomSpawnTime );
    }

    // Update is called once per frame
    public void SpawnNote()
    {
        Instantiate(noteSpawn, transform.position, transform.rotation);
      
            if (stopSpawning) {
                CancelInvoke("SpawnNote");

        }

    }
}
